---
title: Coach Darcy
image_path: https://goo.gl/images/RvUWXP
---
Darcy Knox is the current coach of the Otago Under 15 Boys, as well as the OBHS Junior program. Darcy also has experience in training actual wolves.
