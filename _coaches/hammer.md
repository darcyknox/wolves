---
title: Coach Hammer
image_path: https://goo.gl/images/3B9ycA
---
Hamish is the current head coach of the Otago U17 boys. He is a national champion coach with St Kevins College winning A secondary school tournament in 2015. Hamish bears a strong resemblance to the human knee.
